# pastimes

A list of hobbies, amusements, diversions, leisure activities, and distractions for the bored. Focus is almost entirely on activities that are: 
1. free (or at least fairly inexpensive)
1. solitary

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Project status
Updated randomly.

