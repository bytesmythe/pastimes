# Learn

## About Stuff
+ https://www.khanacademy.org/
+ https://www.youtube.com/user/numberphile
+ https://www.youtube.com/user/1veritasium
+ https://www.youtube.com/user/minutephysics
+ https://www.youtube.com/user/Vsauce
+ https://www.youtube.com/channel/UC1_uAIS3r8Vu6JjXWvastJg (Mathologer)
+ https://www.youtube.com/channel/UC2C_jShtL725hvbm1arSV9w (C.G.P. Grey)
+ https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw (3Blue1Brown)
+ https://www.youtube.com/user/pbsideachannel
+ https://www.youtube.com/channel/UCs4aHmggTfFrpkPcWSaBN9g (PBS Infinite Series)
+ https://www.youtube.com/channel/UC7_gcs09iThXybpVgjHZ_7g (PBS Space Time)
+ https://www.youtube.com/channel/UCAL3JXZSzSm8AlZyD3nQdBA (Primitive Technology)
+ http://tvtropes.org/pmwiki/pmwiki.php/Main/Tropes


## Programming

### Learning

+ https://teachyourselfcs.com/
+ http://greenteapress.com/thinkpython2/html/index.html
+ https://www.reddit.com/r/learnprogramming/
+ https://www.codecademy.com/learn/all
+ https://mitpress.mit.edu/sicp/  (The book used in MIT's intro to programming.  Uses scheme language.)


### Languages
+ https://www.pyret.org/ (Pyret language, a simple, web-based functional language for learning)
+ https://elm-lang.org/ (Elm language for webapp programming)
+ https://kotlinlang.org/ (Kotlin language, primarily for creating Android apps)
+ https://www.freepascal.org/
+ https://racket-lang.org/  (Racket and scheme programming languages)
+ https://elixir-lang.org/ (Robust multi-purpose language that excels at speed, stability, and scalability.)
+ https://nim-lang.org/
+ https://ziglang.org/
+ https://www.unisonweb.org/ (Experimental language that re-imagines the concept of a programming language.)


### Editors
+ https://www.vim.org/ (One of the "one true editors". Not for the faint of heart.)
+ https://neovim.io/  (Updated version of vim.)
+ https://kakoune.org/ (Similar to vim, but commands are reversed so as to be more intuitive. Linux/MacOS/BSD only.)
+ https://www.gnu.org/software/emacs/ (One of the "one true editors". Hope you like key combos.)
+ https://notepad-plus-plus.org/ (Terrific free editor for Windows.)


### Integrated Development Environments
+ http://gambas.sourceforge.net/en/main.html (Free BASIC programming environment.)
+ http://ninja-ide.org/ (python IDE)
+ http://eric-ide.python-projects.org/index.html (python IDE)
+ https://www.kdevelop.org/ (C/C++ IDE)
+ https://www.geany.org/ (C/C++ IDE)
+ http://doc.qt.io/qtcreator/ (C/C++ IDE)
+ http://bluefish.openoffice.nl/index.html (HTML / web development editor)
+ http://www.lazarus-ide.org/  (A free development environment using the Pascal language)


### Puzzles / Challenges / Koans (small puzzles for practicing a programming language)
+ https://adventofcode.com/
+ https://www.hackerrank.com/
+ https://github.com/ahmdrefat/awesome-koans/blob/master/koans-en.md
+ http://elixirkoans.io/


## Electronics / Robotics
+ https://eater.net/
+ https://learn.adafruit.com/category/learn-arduino


## Language / linguistics
+ https://www.duolingo.com/
+ https://www.youtube.com/channel/UCMk_WSPy3EE16aK5HLzCJzw (NativLang)
+ http://www.omniglot.com/
+ http://www.etymonline.com/
+ https://www.clozemaster.com/ (Free vocabulary learning game for several languages)


## Astronomy
+ https://www.telescopesplus.com/products/zhumell-z8-deluxe-dobsonian-reflector-telescope
+ http://www.skyandtelescope.com/astronomy-equipment/how-to-start-right-in-astronomy/
+ http://oneminuteastronomer.com/stargazing-and-night-sky-guide/
+ http://www.astronomyforbeginners.com/
+ https://www.ras.org.uk/education-and-careers/for-everyone/92-getting-started-in-astronomy
+ https://www.space.com/27775-amateur-astronomy-guide.html
+ https://www.space.com/14485-skywatching-telescopes-beginners-guide.html


## Amateur Radio
+ http://www.arrl.org/learning-morse-code 
+ http://c2.com/morse/
+ http://www.g4fon.net/


# Home servers, networking, and automation

## Self-hosting services
+ https://old.reddit.com/r/selfhosted/
+ https://github.com/awesome-selfhosted/awesome-selfhosted


+ http://www.hardkernel.com/main/products/prdt_info.php?g_code=G145457216438 (Odroid C2)
+ https://coreelec.tv/


## Media storage
+ https://blog.linuxserver.io/2017/06/24/the-perfect-media-server-2017/
+ https://blog.linuxserver.io/2019/07/16/perfect-media-server-2019/ (updates to previous)
+ http://www.openmediavault.org/


## Self-hosted data storage
+ https://nextcloud.com/


## Cord cutting
+ https://libreelec.tv/
+ https://nzbget.net/ 
+ https://usenetreviewz.com/nzb-sites/
+ https://sonarr.tv/  (TV shows)
+ https://radarr.video/ (movies)
+ https://github.com/rembo10/headphones/blob/master/README.md (music)


## Run your own "Spotify"-like system
+ https://github.com/airsonic-advanced/airsonic-advanced
+ https://docs.koel.dev/#introduction


# Arts & Crafts

## Beer Making
+ https://www.reddit.com/r/Homebrewing/
+ https://www.homebrewersassociation.org/how-to-brew/beginner/how-to-make-beer/
+ https://www.northernbrewer.com/learn/homebrewing-101/
+ http://howtobrew.com/book/introduction


## Mead Making
+ https://www.reddit.com/r/mead/comments/5kdn5f/how_to_get_started/
+ http://gotmead.com/
+ http://www.stormthecastle.com/mead/mead-recipes.htm
+ https://www.homebrewersassociation.org/how-to-brew/mead/making-mead/
+ https://www.mnn.com/food/beverages/stories/how-make-your-own-honey-mead


## Wine Making 
+ http://eckraus.com/fruit-wine-making/
+ http://www.grit.com/farm-and-garden/fruit-wine-zm0z13jazgou?pageid=1#PageContent1
+ https://www.bottleyourbrand.com/blog/fruit-wine-simple-recipe/
+ http://ediblecapecod.ediblecommunities.com/drink/make-your-own-hillbilly-wine
+ https://thebabblingbotanist.com/2014/11/02/small-batch-homemade-fruit-wines/


## Cheese Making
+ https://cheesemaking.com/blogs/learn/ingredients-for-cheese-making
+ https://www.littlegreencheese.com/
+ https://curd-nerd.com/
+ https://fankhauserblog.wordpress.com/2003/02/02/a-cheese-making-course-syllabus/


## Soap Making
+ https://www.reddit.com/r/soapmaking/
+ https://www.soapqueen.com/bath-and-body-tutorials/cold-process-soap/free-beginners-guide-to-soapmaking-cold-process/


## Chainmail
+ http://www.chainmaildude.com/store/chainmail-kits


## Knotwork / paracord
+ http://www.animatedknots.com/
+ http://www.instructables.com/id/Paracord/
+ https://survivallife.com/36-paracord-projects-preppers/
+ https://www.paracordguild.com/


## Knitting
+ https://www.thespruce.com/learn-to-knit-2116465
+ http://www.learn2knit.co.uk/howtoknit.php
+ https://crafts.tutsplus.com/series/how-to-knit-step-by-step-tutorials-for-beginners--cms-585


## Sewing
+ https://www.thespruce.com/before-you-learn-how-to-sew-2977484


## Carpentry / woodworking
+ http://www.thewoodwhisperer.com/woodworking/for-beginners/
+ http://woodgears.ca/beginner/


## Blacksmithing
+ http://www.popularmechanics.com/home/how-to-plans/how-to/a4087/how-to-make-a-forge/
+ http://www.anvilfire.com/FAQs/getstart/
+ http://beginblacksmithing.com/p/how-to-start-blacksmithing
+ http://www.thewoodwhisperer.com/articles/getting-started-in-blacksmithing/


## Casting aluminum
+ http://www.instructables.com/id/Build-a-foundry-and-sand-cast-aluminum/
+ http://foundry101.com


## Indoor Gardening / Hydroponics
+ http://www.instructables.com/id/Hydroponics---at-Home-and-for-Beginners/
+ https://www.reddit.com/r/hydro/comments/6vming/im_looking_to_get_into_hydroponics_for_an_indoor/


## Writing
+ http://thewritepractice.com/how-to-write-a-short-story/


## Calligraphy
+ https://thepostmansknock.com/beginners-guide-modern-calligraphy/
+ http://thewirecutter.com/reviews/best-pens-and-tools-to-learn-calligraphy/


## Drawing
+ http://drawabox.com/
+ http://www.colourcow.com/picasso-exercise/
+ http://www.artistdaily.com/drawing-basics-learn-to-draw
+ http://thevirtualinstructor.com/pencil-drawing-tutorials.html (has some free lessons)


## Painting
+ https://www.bobross.com/Articles.asp?ID=305  
+ https://www.reddit.com/r/HappyTrees/
+ (I don't care if Bob Ross is a "great master" or not. He makes painting easy and approachable
for a lot of people who might never have tried to learn before.)


## Photography
+ https://www.reddit.com/r/Beginning_Photography/wiki/index
+ https://www.reddit.com/r/analog/wiki/index (Film photography. You remember film, right?)


## Digital Art
+ https://www.blender.org/
+ https://krita.org/en/
+ https://www.ctrlpaint.com/library/


## 3D modeling
+ http://www.wings3d.com/


## Digital audio workstation 
+ https://lmms.io/


## Music composition
+ https://musescore.org/
+ http://lilypond.org/
+ https://qtractor.sourceforge.io/
+ http://www.rosegardenmusic.com/
+ https://www.mixxx.org/wiki/doku.php/list_of_open-source_music_production_software


## Piano
+ https://azpianonews.blogspot.com/2018/10/top-3-best-digital-pianos-usa-reviews-low-price.html
+ https://joshuarosspiano.com/beginner-piano-course/
+ http://pianoforall.com/
+ http://www.flowkey.com/en


## Guitar
+ https://www.justinguitar.com/
+ https://old.reddit.com/r/guitarlessons/
+ https://www.guitarfella.com/best-acoustic-guitar/beginners/
+ https://www.guitarfella.com/best-electric-guitar/beginners/
+ https://www.oldswannerguitartuition.com/  (Paid courses w/ some free info)
+ https://guitarcompass.com/ (Some free lessons)


## Tin Whistle / Recorder
+ http://www.irish-folk-songs.com/learning-tin-whistle.html
+ https://oaim.ie/instrument/1/tin_whistle_beginner_to_advanced
+ http://www.ninenote.com/why_learn_recorder/why_learn_recorder.html
+ https://www.youtube.com/c/SarahJefferyMusic (recorder)
+ https://www.youtube.com/c/CutiePiePipes (tin whistle)


## Harmonica
+ https://www.harmonicalessons.com/beginner-which-harmonica-to-buy.html
+ https://www.harmonica.com/how-to-play-the-harmonica-714.html
+ https://www.harmonica.com/which-harmonica-to-buy-39.html


## Djembe
+ https://www.youtube.com/watch?v=QYQeh5FX-LM (Proper Djembe Technique)


# Sports / Outdoors


## Golf / mini-golf 
+ https://www.reddit.com/r/golf/comments/1j6iwu/my_beginners_guide_to_golf_includes_lots_of/


## Disc golf
+ https://noodlearmdiscgolf.com/beginner-tips/
+ https://www.discraft.com/beginner.html
+ https://www.reddit.com/r/discgolf/comments/iuh0t/just_a_tip_for_all_beginners_seriously/


## Bowling
+ https://www.bowling.com/shopping/all/bowling-balls/entry-performance-balls


## Archery
+ https://www.archery360.com/2016/11/21/gear-up-6-essentials-for-beginning-archers/
+ https://www.archery360.com/archery-101/
+ https://www.bowsports.com/acatalog/Beginners.html
+ https://www.outdoorhub.com/how-to/2011/10/18/beginners-archery-101-basic-instructions/
+ http://www.learn-archery.com/basic-archery.html
+ https://www.archeryworld.co.uk/beginners-information
+ https://www.reddit.com/r/Archery/wiki/guides/beginning


## Darts
+ https://www.reddit.com/r/Darts/


## Hiking / camping
+ https://www.americantrails.org/resources/statetrails/


## Trail / mountain biking
+ https://www.rei.com/learn/expert-advice/mountain-biking-beginners.html
+ https://www.adventurecycling.org/routes-and-maps/adventure-cycling-route-network/
+ http://mountainbiketrailsusa.com/
+ https://www.bicycling.com/bikes-gear/recommended/2016-buyers-guide-best-beginners-bikes


# Games


## Computer games


### Old School PC Games
+ http://www.myabandonware.com/
+ http://www.abandonia.com/
+ http://www.agdinteractive.com/games/games.html (Free remakes of older King's Quest games)
+ http://www.infocom-if.org/downloads/downloads.html (the original Zork games!)


### Emulators
+ https://retropie.org.uk/
+ https://imgur.com/gallery/ekEAY
+ http://mamedev.org/
+ https://www.emuparadise.me/
+ https://www.scummvm.org/  (Emulator for old Sierra Online-style games. You need to find game files to run on it.)
+ http://www.davidkinder.co.uk/frotz.html (Interpreter for Z-machine based games)
+ http://ifdb.tads.org/  (Interactive fiction (text adventure game) database)
+ http://ifdb.tads.org/viewgame?id=ouv80gvsl32xlion (Hitchhiker's Guide to the Galaxy game. You'll need a Z-machine interpreter such as Frotz)
+ http://brasslantern.org/players/howto/tadownload-c.html
+ http://inform7.com/if/interpreters/
+ https://ifcomp.org/ (Interactive fiction contest)


### Other computer games
+ https://play0ad.com/ (Free "civ-like" nation building game)
+ http://www.freeciv.org/ (Another free civilization game)
+ https://kittensgame.com/web/# (Click-and-wait incremental game... Really addictive)


## Chess
+ https://lichess.org/
+ https://www.reddit.com/r/chess/comments/2iqvat/good_youtube_series_to_learn_chess/
+ https://www.chessfactor.com/


## Go
+ https://www.reddit.com/r/baduk/wiki/faq#wiki_where_to_play


## D&D (online!)
+ https://roll20.net/


# Skills

## Juggling
+ http://www.thejugglingscientist.com/learn-to-juggle.html
+ http://libraryofjuggling.com/Tricks/3balltricks/Cascade.html


## Yoyo
+ https://www.yoyosam.com/
+ https://yoyotricks.com/
+ http://yoyo.wikia.com/wiki/Main_Page
+ https://www.youtube.com/channel/UC6qYGx_P4jH6hwCW2wjAygQ (yotricks)


## Kites
+ http://www.nationalkitemonth.org/how-to-fly-a-kite/
+ http://www.windpowersports.com/


## Footbag
+ http://worldfootbag.com/product-category/footbags/
+ https://hittinthesack.wordpress.com/2011/10/25/tips-for-footbag-beginners/


## Cube puzzles
+ https://speedcubeshop.com/
+ https://thecubicle.us/
+ https://www.reddit.com/r/Cubers/wiki/which_cubes_to_buy
+ https://ruwix.com/
+ http://www.howtocube.com/
+ http://www.ryanheise.com/cube/solutions.html
+ https://www.youtube.com/watch?v=609nhVzg-5Q (algorithm method)
+ https://www.youtube.com/watch?v=9Za5PhDBpQQ
+ https://www.youtube.com/watch?v=3NYzeX8eE_4
+ https://www.reddit.com/r/Speedcubing/
+ https://www.speedsolving.com/
+ http://lar5.com/cube/  (speed solving method)


## Magic
+ https://www.reddit.com/r/Magic/wiki/index
+ https://www.amazon.com/Royal-Road-Card-Magic/dp/1987817893
+ https://www.amazon.com/Card-Manipulations-Dover-Magic-Books/dp/0486205398/
+ https://www.amazon.com/Expert-Card-Table-Treatise-Manipulation/dp/0486285979/
+ https://www.amazon.com/Modern-Coin-Magic-Sleights-Tricks/dp/0486242587/
+ https://www.amazon.com/Modern-Magic-Makers-Tricks-Moves/dp/B000JQQKJA/


## Lockpicking
+ https://www.reddit.com/r/lockpicking/wiki/generalwiki (Nearly everything you'd want to read is listed here.)


## Domino (Knocking down)
+ https://picklebums.com/dominoes/
+ http://www.domino-play.com/TopplingBasic.htm
+ https://bulkdominoes.com/
